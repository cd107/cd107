#include<stdio.h>
void swapnum(int *x,int *y)
{
    int tempnum;
    tempnum=*x;
    *x=*y;
    *y=tempnum;
}
int main()
{
    int a=20, b=40;
    printf("Before swapping \n");
    printf("First number is %d\n",a);
    printf("Second number is %d\n",b);
    swapnum(&a,&b);
    printf("After swapping\n");
    printf("First number is %d\n",a);
    printf("Second number is %d\n",b);
    return 0;
}