#include<stdio.h>
typedef struct employee{
    char name[20];
    int eid;
    float salary;
    char cname[50];
}employee;

void input()
{
    struct employee e1;
    printf("Enter employee name\n");
    scanf("%s",e1.name);
    printf("Enter employee id\n");
    scanf("%d",&e1.eid);
    printf("Enter employee salary\n");
    scanf("%f",&e1.salary);
    printf("Enter employee company name\n");
    scanf("%s",e1.cname);
}

void display()
{
    struct employee e1;
    printf("Employee name is: %s\n",e1.name);
    printf("Employee id is: %d\n",e1.eid);
    printf("Employee salary is: %f\n",e1.salary);
    printf("Employee company name is: %s\n",e1.cname);
}

int main()
{
    struct employee e1;
    input(&e1);
    display(&e1);
    return 0;
}