#include<stdio.h>
int input()
{
	int n;
	printf("Enter a no.");
	scanf("%d",&n);
	return n;
}
int compute(int n)
{
	int r,i,sum=0;
	for(i=1;i<=n;i++)
	{
		r=n%10;
		sum=sum+r;
		n=n/10;
	}
	return sum;
}
void display(int n,int s)
{
	printf("Sum of digits of the entered number %d is=%d",n,s);
}
int main()
{
	int n,sum;
	n=input();
	sum=compute(n);
	display(n,sum);
	return 0;
}
