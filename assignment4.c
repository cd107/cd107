#include<stdio.h>
int input()
{
	int a;
	printf("Enter a no.\n");
	scanf("%d",&a);
	return a;
}
int compute(int n)
{
	int i,fact=1;
	for(i=1;i<=n;i++)
	{
		fact=fact*i;
	}
	return fact;
}
void display(int n)
{
	printf("Factorial is %d\n",n);
}
int main()
{
	int n,f;
	n=input();
	f=compute(n);
	display(f);
	return 0;
}
