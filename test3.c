#include<stdio.h>
int input()
{
	int a;
	printf("Enter radius of circle");
	scanf("%d",&a);
	return a;
}
float compute(int r)
{
	float pi=3.14;
	float area;
	area=pi*r*r;
	return area;
}
void display(float a)
{
	printf("area of the circle is %f",a);
}
int main()
{
	int a;
	float area;
	a=input();
	area=compute(a);
	display(area);
	return 0;
}
