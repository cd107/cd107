#include<stdio.h>
#include<math.h>
int input()
{
	int a;
	printf("Enter sides of triangle\n");
	scanf("%d",&a);
	return a;
}
float compute(int a,int b,int c)
{
	float s,area;
	s=(a+b+c)/2;
	area=sqrt(s*(s-a)*(s-b)*(s-c));
	return area;
}
void display(float a)
{
	printf("Area of triangle is %f\n",a);
}
int main()
{
	int a,b,c;
	float area;
	a=input();
	b=input();
	c=input();
	area=compute(a,b,c);
	display(area);
	return 0;
}
