#include<stdio.h>
#include<math.h>
int input()
{
	int n;
	printf("Enter value of n\n");
	scanf("%d",&n);
	return n;
}
int compute(int x)
{
	int i,p,sum=0;
	for(i=1;i<=x;i++)
	{
		if(i%2==0)
		{
			p=pow(i,2);
			sum=sum+p;
		}
	}
	return sum;
}
void display(int n)
{
	printf("The sum is %d\n",n);
}
int main()
{
	int n,sum;
	n=input();
	sum=compute(n);
	display(sum);
	return 0;
}