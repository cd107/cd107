#include<stdio.h>
#include<math.h>
typedef struct complex{
    float real;
    float imag;
}complex;

void input(int *a, int *b, int *c)
{
	printf("Enter the value of a,b,c\n");
	scanf("%d%d%d",&(*a),&(*b),&(*c));
}
void compute(int a,int b,int c, complex *r1, complex *r2)
{
	float d,x1,x2;
	d=(b*b)-(4*a*c);
	if (d>0)
	{
		x1=(-b+sqrt(d))/(2*a);
		x2=(-b-sqrt(d))/(2*a);
		r1->real=x1;
		r1->imag=0;
		r2->real=x2;
		r2->imag=0;
		
	}
	else if (d==0)
	{
		x1=x2=(-b+sqrt(d))/(2*a);
		r1->real=r2->real=x1=x2;
		r1->imag=r2->imag=0;
		
	}
	else
	{
	    r1->real=-b/(2*a);
	    r1->imag=+sqrt(-d)/(2*a);
	    r2->real=-b/(2*a);
	    r2->imag=-sqrt(-d)/(2*a);
	}
		
}
void display(int a,int b,int c,complex r1, complex r2)
{
    printf("The roots of the equation %dx^2+%dx+%d is %f+i%f\n",a,b,c,r1.real,r1.imag);
    printf("The roots of the equation %dx^2+%dx+%d is %f+i%f\n",a,b,c,r2.real,r2.imag);

}
int main()
{
	complex x,y;
	int a,b,c;
	input(&a,&b,&c);
	compute(a,b,c,&x,&y);
	display(a,b,c,x,y);
	return 0;
}