#include<stdio.h>
int main()
{
    int i,j,m,n, first[10][10], second[10][10], sum[10][10], difference[10][10];
    printf("Enter the number of rows and columns of a matrix\n");
    scanf("%d%d",&m,&n);
    printf("Enter the elements of first array\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
            scanf("%d",&first[i][j]);
    }
    printf("Enter the elements of second array\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
            scanf("%d",&second[i][j]);
    }
    printf("The sum of the array is\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            sum[i][j]=first[i][j]+second[i][j];
            printf("%d\t",sum[i][j]);
        }
        printf("\n");
    }
    printf("The difference of the array is\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            difference[i][j]=first[i][j]-second[i][j];
            printf("%d \t",difference[i][j]);
        }
        printf("\n");
    }
    return 0;
}