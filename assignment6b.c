#include<stdio.h>
#include<math.h>
int input()
{
    int n;
    printf("Enter value of n\n");
    scanf("%d",&n);
    return n;
}
float compute(int x)
{
    float i,p;
    float sum=0;
    for(i=1;i<=x;i++)
    {
        p=pow(i,2);
        sum=sum+(1/p);
    }
return sum;
}
void display(float x)
{
    printf("The sum is %f\n",x);
}
int main()
{
    int n;
    float sum;
    n=input();
    sum=compute(n);
    display(sum);
    return 0;
}
