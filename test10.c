#include<stdio.h>
void add(int *a, int *b, int *s);
void sub(int *a, int *b, int *k);
void mul(int *a, int *b, int *m);
void div(int *a, int *b, int *d);
void rem(int *a, int *b, int *r);
int main()
{
	int x,y,A,S,M,D,R;
	printf("Enter two numbers\n");
	scanf("%d%d",&x,&y);
	add(&x,&y,&A);
	printf("Sum of two numbers %d+%d=%d\n",x,y,A);
	sub(&x,&y,&S);
	printf("Subtraction of two numbers %d-%d=%d\n",x,y,S);
	mul(&x,&y,&M);
	printf("Multiplication of two numbers %d*%d=%d\n",x,y,M);
	div(&x,&y,&D);
	printf("Division of two numbers %d/%d=%d\n",x,y,D);
	rem(&x,&y,&R);
	printf("Remainder of two numbers (%d) % (%d) =%d\n",x,y,R);
	return 0;
}
void add(int *a, int *b, int *s)
{
	*s=(*a)+(*b);
}
void sub(int *a, int *b, int *k)
{
	*k=(*a)-(*b);
}
void mul(int *a, int *b, int *m)
{
	*m=(*a)*(*b);
}
void div(int *a, int *b, int *d)
{
	*d=(*a)/(*b);
}
void rem(int *a, int *b, int *r)
{
	*r=(*a)%(*b);
}