#include<stdio.h>
typedef struct student{
    char name[20];
    int rno;
    char section[20];
    char dept[50];
    float fees;
    float marks;
}student;

void input(student *s1, student *s2)
{
    printf("Enter first student details\n");
    printf("Enter student name\n");
    scanf("%s",s1->name);
    printf("Enter student rno\n");
    scanf("%d",&s1->rno);
    printf("Enter student section\n");
    scanf("%s",s1->section);
    printf("Enter student dept\n");
    scanf("%s",s1->dept);
    printf("Enter student fees\n");
    scanf("%f",&s1->fees);
    printf("Enter max marks obtained by student\n");
    scanf("%f",&s1->marks);
    printf("Enter second student details\n");
    printf("Enter student name\n");
    scanf("%s",s2->name);
    printf("Enter student rno\n");
    scanf("%d",&s2->rno);
    printf("Enter student section\n");
    scanf("%s",s2->section);
    printf("Enter student dept\n");
    scanf("%s",s2->dept);
    printf("Enter student fees\n");
    scanf("%f",&s2->fees);
    printf("Enter max marks obtained by student\n");
    scanf("%f",&s2->marks);
}
student compute(student s1, student s2)
{
    if(s1.marks>s2.marks)
    return s1;
    else
    return s2;
}

void display(student s)
{
    printf("Student name is: %s\n",s.name);
    printf("Student rno is: %d\n",s.rno);
    printf("Student section is: %s\n",s.section);
    printf("Student department is: %s\n",s.dept);
    printf("Student fees is: %f\n",s.fees);
    printf("Max marks obtained by student is:%f \n",s.marks);
}

int main()
{
    struct student s1,s2,p;
    input(&s1,&s2);
    p=compute(s1,s2);
    display(p);
    return 0;
}